import argparse
import re

TIME_FORMAT = r"(\d{2}):(\d{2}):(\d{2}),(\d{3})"


def change_time(time_tuple, seconds_to_change):
    # not sure if there are some nice time packages here, but it's simple enough
    s_change = int(seconds_to_change)

    h, m, s, ms = time_tuple
    h = int(h)
    m = int(m)
    s = int(s)
    ms = int(ms)

    s += s_change
    if s > 59:
        m += 1
        s -= 60
        if m > 59:
            h += 1  # ignoring h conditions here
            m -= 60
    elif s < 0:
        m -= 1
        s += 60
        if m < 0:
            h -= 1
            m += 60

    return str(h).zfill(2) + ":" + str(m).zfill(2) + ":" + str(s).zfill(2) + "," + str(ms).zfill(3)


def parse_line(line, seconds_to_change):
    time_exist = re.findall(TIME_FORMAT, line)
    if time_exist:
        new_line = change_time(time_exist[0], seconds_to_change) + " --> " + change_time(time_exist[1],
                                                                                         seconds_to_change) + "\n"
    else:
        new_line = line

    return new_line


def main(input_filename, seconds_to_change):
    output_filename = "new_" + input_filename
    with open(input_filename, "r") as input_file:
        with open(output_filename, "w") as output_file:
            for l in input_file:
                new_line = parse_line(l, seconds_to_change)
                output_file.write(new_line)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("-f", "--file", help="name of srt file", type=str)
    parser.add_argument("-s", "--seconds", help="seconds to add or subtract", type=str)
    args = parser.parse_args()
    main(args.file, args.seconds)
